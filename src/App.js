import React from 'react'
import 'tachyons'
import 'styling/semantic.less'

import CopyTester from './views/pages/CopyTester'

const App = () =>
    <CopyTester />

export default App

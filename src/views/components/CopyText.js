import React from 'react'
import PropTypes from 'prop-types'
import { Segment } from 'semantic-ui-react'
import { map } from 'lodash'

const CopyText = ({ paragraphs }) => (
    <Segment>
        {map(paragraphs, (text, index) => <p key={index}>{text}</p>)}
    </Segment>
)

CopyText.propTypes = {
    paragraphs: PropTypes.arrayOf(PropTypes.string).isRequired,
}

export default CopyText

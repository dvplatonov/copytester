import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Container, Button, Icon } from 'semantic-ui-react'
import { map, reduce, find, memoize, dropRight, isNil } from 'lodash/fp'

const byPath = (menuPath, menu) =>
    reduce(
        (acc, currentId) =>
            isNil(acc)
                ? null
                : find(item => item.id === currentId, acc.submenu),
        menu,
        menuPath
    )

const addToPath = memoize((menuId, selectedPath, setSelectedPath) => () => {
    setSelectedPath([...selectedPath, menuId])
})

const removeOneFromPath = memoize((selectedPath, setSelectedPath) => () => {
    setSelectedPath(dropRight(1, selectedPath))
})

const ButtonMenu = ({ menu }) => {
    const [selectedPath, setSelectedPath] = useState([])
    const { submenu, label } = byPath(selectedPath, menu)

    return (
        <Container>
            {label === null ? null : `${label}`}
            {label && submenu && submenu.length > 0 ? ': ' : ''}
            {map(
                ({ id, label }) => (
                    <Button
                        basic
                        key={id}
                        onClick={addToPath(id, selectedPath, setSelectedPath)}
                    >
                        {label}
                    </Button>
                ),
                submenu
            )}
            {selectedPath.length === 0 ? null : (
                <Icon
                    link
                    name="close"
                    onClick={removeOneFromPath(selectedPath, setSelectedPath)}
                />
            )}
        </Container>
    )
}

ButtonMenu.propTypes = {
    menu: PropTypes.object.isRequired,
}

export default ButtonMenu

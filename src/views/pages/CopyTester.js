import React from 'react'
import CopyText from '../components/CopyText'
import ButtonMenu from '../components/ButtonMenu'

const loremIpsum = [
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
]
const menu = {
    id: 'root',
    label: null,
    submenu: [
        {
            id: 'unclear',
            label: 'Unclear',
            submenu: [
                {
                    id: 'jargon',
                    label: 'Jargon',
                },
                {
                    id: 'tooLong',
                    label: 'Too long',
                },
                {
                    id: 'dontGet',
                    label: "Don't get it",
                },
            ],
        },
        {
            id: 'interesting',
            label: 'Interesting',
            submenu: [],
        },
        {
            id: 'boring',
            label: 'Boring',
            submenu: [],
        },
    ],
}

const CopyTester = () => (
    <div className="min-vh-100 w-100 flex flex-column items-center justify-center">
        <div className="w-100 mw7">
            <CopyText paragraphs={loremIpsum} />
            <ButtonMenu menu={menu} />
        </div>
    </div>
)

export default CopyTester
